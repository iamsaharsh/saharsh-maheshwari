
# coding: utf-8

# In[345]:


import pandas as pd
import os
import numpy as np
import datetime as dt
import warnings
warnings.filterwarnings("ignore")
import tldextract
import re
import nltk
import json
from bs4 import BeautifulSoup
import pandas as pd
import re
import itertools
from datetime import datetime
from nltk.corpus import stopwords
from textblob import TextBlob
stop = stopwords.words('english')
from nltk.stem import PorterStemmer
port = PorterStemmer()
stop = stopwords.words('english')


# In[346]:


#Set the parent directory
os.chdir(r'C:\Users\saharsh.maheshwari\Downloads')


# In[412]:


#Load the input file into a DataFrame
my_df = pd.read_excel(r"C:\Users\saharsh.maheshwari\Downloads\upload.xlsx", encoding= 'latin-1')


# In[413]:


my_df.head()


# In[349]:


#function for removing tags and hex codes
def clean_data_1(text):
    soup = BeautifulSoup(text,"lxml")
    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.decompose()    # rip it out
    # get text
    text = soup.get_text()
    txt=text.encode('ascii','replace')
    txt=str(txt).replace("\\n"," ") 
    txt=str(txt).replace("?","")
    text=re.sub(r'^(b\")|^(b\')',"",txt)
    return text


# In[378]:


#Function for carrying out the basic process of the input data
def preprocess(text): #takes the input for cleaning
    start_time = datetime.now() #logs in the time at which the function is started
    text2 = []
    x=0
    for i in range(len(text)): # loop for taking each row of the input data and processing it
        try:
            text1 = str(text[i]) #converted into string format
            text1 = re.sub(r'\b[\w\-.]+?@\w+?\.\w{2,4}\b','emailaddr',text1) #email addresses handled
            text1 = re.sub(r'(http[s]?\S+)|(\w+\.[A-Za-z]{2,4}\S*)','httpaddr',text1)#website links handeled
            text1 = re.sub(r'[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"]',"",text1[i]) # special characters removed
            text1 = text1.lower() # text case handeled
            text1 = re.sub(r"(http)\w*"," ",text1) #http tags removed
            text1 = re.sub(r"\bamp\b"," ",text1) #remaining tags handled
            text1 = re.sub("\d+", "", text1) #Removing digits
            text1 = re.sub(r"\s+"," ",text1) #whitespaces handled
            text1 = ''.join(''.join(s)[:2] for _, s in itertools.groupby(text1)) #elongated/exagerated words handled
            text1 = re.sub(r'([A-Z])', r' \1', text1) #Joint words splitted
            text1 = ' '.join([word for word in text1.split() if word not in (stop)]) #stopwords removed
            text2.append(text1)   #Processed text appended to empty list 
        except(TypeError): #exception command incase of type error
             text2.append(text[i])
        x = x+1
        if (x%1000==0): #row count iterator
            print('lookup iterator is ',x)
    end_time = datetime.now() #logs in the time at which the function gets completed
    print('Duration: {}'.format(end_time - start_time)) #gives the time taken for completion
    return text2 #returns the output list


# In[408]:


def preprocess1(text): #takes the input for cleaning
    start_time = datetime.now() #logs in the time at which the function is started
    text2 = []
    x=0
    try:
        text1 = str(text) #converted into string format
        text1 = re.sub(r'\b[\w\-.]+?@\w+?\.\w{2,4}\b','emailaddr',text1) #email addresses handled
        text1 = re.sub(r'(http[s]?\S+)|(\w+\.[A-Za-z]{2,4}\S*)','httpaddr',text1)#website links handeled
        text1 = text1.split()
        for i in range(len(text1)):
            if(text1[i]== r'\b[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"]\b'):     
                re.sub(r'\b[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"]\b',"") 
            elif(text1[i]==r'#|@[a-zA-Z]+\b'):
                pass
                #re.sub(r'\b[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"]\b',"",text1[i]) # special characters removed
        text1 = ' '.join(text1)           
        text1 = text1.lower() # text case handeled
        text1 = re.sub(r"(http)\w*"," ",text1) #http tags removed    r'(\b#|@[a-zA-Z]+\b)'
        text1 = re.sub(r"\bamp\b"," ",text1) #remaining tags handled
        text1 = re.sub("\d+", "", text1) #Removing digits
        text1 = re.sub(r"\s+"," ",text1) #whitespaces handled
        text1 = ''.join(''.join(s)[:2] for _, s in itertools.groupby(text1)) #elongated/exagerated words handled
        text1 = re.sub(r'([A-Z])', r' \1', text1) #Joint words splitted
        text1 = ' '.join([word for word in text1.split() if word not in (stop)]) #stopwords removed
        text2.append(text1)   #Processed text appended to empty list 
    except(TypeError): #exception command incase of type error
        text2.append(text)
        x = x+1
        if (x%1000==0): #row count iterator
            print('lookup iterator is ',x)
    end_time = datetime.now() #logs in the time at which the function gets completed
    print('Duration: {}'.format(end_time - start_time)) #gives the time taken for completion
    return text2 #returns the output list


# In[409]:


preprocess1('#love soooo gooo#sss @good')


# In[379]:


preprocess(my_df1.preprocessed)


# In[351]:


#Function for stemming the processed text
def stemdistuff(text_list): #takes the input for stemming 
    start_time = datetime.now() #logs in the time at which the function is started
    content=[]
    x=0
    for i in range(0,len(text_list)): # loop for taking each row of the input data and processing it
        try:
            content_name = ' '.join([port.stem(m) for m in text_list[i].split()]) #splitting the text, stemming it, joining it again
            content.append(content_name)
        except(TypeError):
            content.append(text_list[i])
        x = x+1
        if (x%1000==0): #row count iterator
            print('lookup iterator is ',x)
    end_time = datetime.now() #logs in the time at which the function gets completed
    print('Duration: {}'.format(end_time - start_time))  #gives the time taken for completion
    return content #returns the output list


# In[354]:


cnt=[]
i=0
start_time = datetime.now()

for each in my_df[0]: 
    try:
        
        text=each
        txt=clean_data_1(text)
        cnt.append(txt)
        
    except(TypeError):
        cnt.append('NA')
    i=i+1
    if(i%1000==0):
        print(i)
my_df['parsed'] = cnt
        
end_time = datetime.now()    
        
print(start_time)
print(end_time)
print('Duration: {}'.format(end_time - start_time))


# In[115]:


my_df1 = my_df[1:10]


# In[110]:


my_df1.head()


# In[360]:


my_df['preprocessed'] = preprocess(my_df.parsed)


# In[356]:


my_df['preprocessed'] = stemdistuff(my_df.preprocessed)


# In[361]:


my_df.to_excel('upload.xlsx')


# In[436]:


from sklearn.feature_extraction.text import TfidfVectorizer
vectorizer = TfidfVectorizer(max_features= 10000)


# In[437]:


def Tf_Idf_process(text):
    print("Tf-Idf Starting")
    vector = vectorizer.fit_transform(text.values.astype('U'))  ## Even astype(str) would work
    d = dict(zip(vectorizer.get_feature_names(), vector.data))
    d1 = sorted(d.items(), key=lambda x: x[1],reverse=True)
    return d1


# In[424]:


type(x)


# In[ ]:


x = []


# In[438]:


x = Tf_Idf_process(my_df.Lemmatized)


# In[447]:


def build_vocab_Tf_Idf(text,limit):
    #word_counts = Counter(itertools.chain(*text))
    vocabulary_inv = ["<PAD/>"] + [word[0] for word in text];
    vocabulary = {word: index for index, word in enumerate(vocabulary_inv)}
    return vocabulary


# In[448]:


y = build_vocab_Tf_Idf(x,15)


# In[449]:


y


# In[428]:


c = json.dumps(y)


# In[429]:


type(c)


# In[430]:


c


# In[450]:


with open('vocab.json', 'w') as outfile:
    json.dump(y,outfile)

