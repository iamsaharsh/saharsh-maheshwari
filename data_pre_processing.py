import json
from bs4 import BeautifulSoup
import pandas as pd
import re
from datetime import datetime
from numpy import dot
from numpy.linalg import norm
import spacy
from spacy.lang.en import English
import os
import numpy as np
import warnings
import itertools
warnings.filterwarnings("ignore")
import re
import nltk
from nltk.corpus import stopwords
from textblob import TextBlob
stop = stopwords.words('english')
from nltk.stem import PorterStemmer
port = PorterStemmer()
from keras.models import model_from_json
from keras.preprocessing import sequence
from preprocessor import clean_str


	class process_basic(text):#takes the input for cleaning
        def __init__(text):
    	
            print("process_basic starting")
    	
    	    text1 = ""
	        try:
	        text1 = str(text) #converted into string format
	        text1 = re.sub(r'\b[\w\-.]+?@\w+?\.\w{2,4}\b','emailaddr',text1) #email addresses handled
	        text1 = re.sub(r'(http[s]?\S+)|(\w+\.[A-Za-z]{2,4}\S*)','httpaddr',text1) #website links handeled
	        #text1 = re.sub(r'[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"]',"",text1) # special characters removed
			text1 = text1.split()
			for i in range(len(text1)):
				if (text1[i] == r'#|@[a-zA-Z]+\b'):    #regex needs to be modified
					pass
				else:
					re.sub(r'\b[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"]\b', "", text1[i])
			text1 = ' '.join(text1)
	        text1 = re.sub(r'([A-Z])', r' \1', text1) #Joint words splitted
	        text1 = text1.lower() # text case handeled
	        text1 = re.sub(r"(http)\w*"," ",text1) #http tags removed
	        text1 = re.sub(r"\bamp\b"," ",text1) #remaining tags handled
	        text1 = re.sub(r"\s+"," ",text1) #whitespaces handled
	        text1 = re.sub("\d+", "", text1) #Removing digits
	        text1 = ''.join(''.join(s)[:2] for _, s in itertools.groupby(text1)) #elongated/exagerated words handled
	        text1 = ' '.join([word for word in text1.split() if word not in (stop)]) #stopwords removed
	        text1  #Processed text appended to empty list
	    	except(TypeError): #exception command incase of type error
	        	 text1= text
	  			 print("process_basic finished")
	    
		return text1
	     
	class stemming(text):
		def __init__(text): #takes the input for stemming
	   		content= ""
	    	try:
	        content = ' '.join([port.stem(m) for m in text.split()]) #splitting the text, stemming it, joining it again
	       
	   	 	except(TypeError):
	        content = text
	    return content 

